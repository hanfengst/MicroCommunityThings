/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.api.extApi.tempCarAuth;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.entity.car.BarrierGateControlDto;
import com.java110.core.service.car.ICarInoutTempAuthService;
import com.java110.core.service.community.ICommunityService;
import com.java110.core.service.parkingArea.IParkingAreaService;
import com.java110.core.service.parkingCouponCar.IParkingCouponCarService;
import com.java110.core.util.Assert;
import com.java110.core.util.BeanConvertUtil;
import com.java110.core.util.SeqUtil;
import com.java110.entity.car.CarInoutTempAuthDto;
import com.java110.entity.community.CommunityDto;
import com.java110.entity.parkingArea.ParkingAreaDto;
import com.java110.entity.parkingCouponCar.ParkingCouponCarDto;
import com.java110.entity.response.ResultDto;
import com.java110.intf.inner.ICallCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 临时车 审核接口
 * add by 吴学文 2021-01-17
 * <p>
 * 该接口类为 需要将临时车费用等信息 同步时需要调用
 */

@RestController
@RequestMapping(path = "/extApi/tempCarAuth")
public class TempCarAuthExtController {

    @Autowired
    ICommunityService communityServiceImpl;

    @Autowired
    private IParkingAreaService parkingAreaServiceImpl;

    @Autowired
    private ICarInoutTempAuthService carInoutTempAuthServiceImpl;

    @Autowired
    private ICallCarService callCarServiceImpl;

    /**
     * 添加或修改 停车卷
     * <p>
     *
     * @param reqParam
     * @return 成功或者失败
     * @throws Exception
     */
    @RequestMapping(path = "/getCarInoutTempAuths", method = RequestMethod.POST)
    public ResponseEntity<String> getCarInoutTempAuths(@RequestBody String reqParam) throws Exception {
        JSONObject reqJson = JSONObject.parseObject(reqParam);

        Assert.hasKeyAndValue(reqJson, "page", "未包含分页信息");
        Assert.hasKeyAndValue(reqJson, "row", "未包含行");
        Assert.hasKeyAndValue(reqJson, "extCommunityId", "未包含外部小区ID");
        Assert.hasKeyAndValue(reqJson, "extPaId", "未包含停车场ID");

        CommunityDto communityDto = new CommunityDto();
        communityDto.setExtCommunityId(reqJson.getString("extCommunityId"));
        ResultDto resultDto = communityServiceImpl.getCommunity(communityDto);

        List<CommunityDto> communityDtos = (List<CommunityDto>) resultDto.getData();

        Assert.listOnlyOne(communityDtos, "未找到小区信息");

        ParkingAreaDto parkingAreaDto = new ParkingAreaDto();
        parkingAreaDto.setExtPaId(reqJson.getString("extPaId"));
        parkingAreaDto.setCommunityId(communityDtos.get(0).getCommunityId());
        List<ParkingAreaDto> parkingAreaDtos = parkingAreaServiceImpl.queryParkingAreas(parkingAreaDto);

        Assert.listOnlyOne(parkingAreaDtos, "未找到停车场信息");


        CarInoutTempAuthDto carInoutTempAuthDto = BeanConvertUtil.covertBean(reqJson, CarInoutTempAuthDto.class);
        carInoutTempAuthDto.setCommunityId(communityDtos.get(0).getCommunityId());
        carInoutTempAuthDto.setPaId(parkingAreaDtos.get(0).getPaId());

        long count = carInoutTempAuthServiceImpl.queryCarInoutTempAuthCount(carInoutTempAuthDto);

        List<CarInoutTempAuthDto> carInoutTempAuthDtos = null;
        if (count > 0) {
            carInoutTempAuthDtos = carInoutTempAuthServiceImpl.queryCarInoutTempAuth(carInoutTempAuthDto);
        } else {
            carInoutTempAuthDtos = new ArrayList<>();
        }

        ResultDto resultVo = new ResultDto(ResultDto.SUCCESS, "成功", carInoutTempAuthDtos);
        resultVo.setTotal(count);
        resultVo.setTotalPage((int) Math.ceil((double) count / (double) carInoutTempAuthDto.getRow()));
        return ResultDto.createResponseEntity(resultVo);
    }


    /**
     * 审核进场
     * <p>
     *
     * @param reqParam
     * @return 成功或者失败
     * @throws Exception
     */
    @RequestMapping(path = "/updateCarInoutTempAuths", method = RequestMethod.POST)
    public ResponseEntity<String> updateCarInoutTempAuths(@RequestBody String reqParam) throws Exception {
        JSONObject reqJson = JSONObject.parseObject(reqParam);
        Assert.hasKeyAndValue(reqJson, "authId", "未包含外部Id");
        Assert.hasKeyAndValue(reqJson, "state", "未包含状态");
        Assert.hasKeyAndValue(reqJson, "msg", "未包含审核说明");
        CarInoutTempAuthDto carInoutTempAuthDto = new CarInoutTempAuthDto();
        carInoutTempAuthDto.setAuthId(reqJson.getString("authId"));
        carInoutTempAuthDto.setState(CarInoutTempAuthDto.STATE_W);
        List<CarInoutTempAuthDto> carInoutTempAuthDtos = carInoutTempAuthServiceImpl.queryCarInoutTempAuth(carInoutTempAuthDto);

        Assert.listOnlyOne(carInoutTempAuthDtos, "临时车审核不存在或者已审核");

        carInoutTempAuthDto = BeanConvertUtil.covertBean(reqJson, CarInoutTempAuthDto.class);
        int flag = carInoutTempAuthServiceImpl.updateCarInoutTempAuth(carInoutTempAuthDto);

        if (flag < 1) {
            return ResultDto.error("失败");
        }

        if (!CarInoutTempAuthDto.STATE_C.equals(carInoutTempAuthDto.getState())) {
            return ResultDto.success();
        }

        callCarServiceImpl.tempCarAuthOpen(carInoutTempAuthDtos.get(0));

        return ResultDto.success();
    }
}
